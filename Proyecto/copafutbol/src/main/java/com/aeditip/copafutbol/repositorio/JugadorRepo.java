package com.aeditip.copafutbol.repositorio;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.aeditip.copafutbol.entidad.Jugador;

@Repository
public interface JugadorRepo extends JpaRepository<Jugador, Integer>  {
}
