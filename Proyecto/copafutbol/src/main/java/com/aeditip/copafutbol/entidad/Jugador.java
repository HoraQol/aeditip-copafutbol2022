package com.aeditip.copafutbol.entidad;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

@Entity
@Table(name = "jugador")
public class Jugador {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name="id")
    private int id;

    @Column(name="nombre")
    private String nombre;

    @Column(name="dorsal")
    private int dorsal;

    @Column(name="posicion")
    private String posicion;

    @ManyToOne
    @JoinColumn(name="id_pais")
    private Pais pais;

    /* Constructores */
    public Jugador() {}

    public Jugador(int id, String nombre, int dorsal, String posicion, Pais pais) {
        this.id = id;
        this.nombre = nombre;
        this.dorsal = dorsal;
        this.posicion = posicion;
        this.pais = pais;
    }

    /* Encapsulamiento */
    public int getId(){
        return id;
    }

    public void setId(int id){
        this.id = id;
    }

    public String getNombre(){
        return nombre;
    }

    public void setNombre(String nombre){
        this.nombre = nombre;
    }
    
    public int getDorsal(){
        return dorsal;
    }

    public void setDorsal(int dorsal){
        this.dorsal = dorsal;
    }

    public String getPosicion(){
        return posicion;
    }

    public void setPosicion(String posicion){
        this.posicion = posicion;
    }

    public Pais getPais(){
        return pais;
    }

    public void setPais(Pais pais){
        this.pais = pais;
    }
}
