package com.aeditip.copafutbol.servicio;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.aeditip.copafutbol.entidad.Jugador;
import com.aeditip.copafutbol.entidad.Pais;
import com.aeditip.copafutbol.repositorio.JugadorRepo;
import com.aeditip.copafutbol.repositorio.PaisRepo;

@Service
public class JugadorServicio {
    @Autowired
    private JugadorRepo repo;
    @Autowired
    private PaisRepo paisRepo;

    /* Crear */
    public Jugador crearJugador(Jugador jugador, String pais){
        Pais auxPais = paisRepo.findById(pais).orElse(null);
        if(!auxPais.equals(null)){
            jugador.setPais(auxPais);
            return repo.save(jugador);
        }
        return null;
    }

    /* Leer */
    public Jugador leerJugador(int id){
        return repo.findById(id).orElse(null);
    }

    public List<Jugador> leerTodosJugadores(){
        return repo.findAll();
    }
}
