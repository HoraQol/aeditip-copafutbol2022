package com.aeditip.copafutbol;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class CopafutbolApplication {

	public static void main(String[] args) {
		SpringApplication.run(CopafutbolApplication.class, args);
	}

}
