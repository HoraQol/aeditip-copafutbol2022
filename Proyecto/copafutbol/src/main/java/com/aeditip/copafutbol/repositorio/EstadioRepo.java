package com.aeditip.copafutbol.repositorio;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.aeditip.copafutbol.entidad.Estadio;

@Repository
public interface EstadioRepo extends JpaRepository<Estadio, Integer> {
}
