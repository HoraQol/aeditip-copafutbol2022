package com.aeditip.copafutbol.entidad;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

@Entity
@Table(name = "gol")
public class Gol {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name="id")
    private int id;

    @ManyToOne
    @JoinColumn(name="id_partido")
    private Partido partido;

    @Column(name="tipo")
    private String tipo;

    @ManyToOne
    @JoinColumn(name="id_anotador")
    private Jugador anotador;

    @ManyToOne
    @JoinColumn(name="id_asistidor")
    private Jugador asistidor;

    @Column(name="penal")
    private int penal;

    @Column(name="minuto")
    private int minuto;

    @Column(name="tiempo")
    private int tiempo;

    /* Constructores */
    public Gol() {}

    public Gol(int id, Partido partido, String tipo, Jugador anotador, Jugador asistidor,
            int penal, int minuto, int tiempo) {
        this.id = id;
        this.partido = partido;
        this.tipo = tipo;
        this.anotador = anotador;
        this.asistidor = asistidor;
        this.penal = penal;
        this.minuto = minuto;
        this.tiempo = tiempo;
    }

    /* Encapsulamiento */
    public int getId(){
        return id;
    }

    public void setId(int id){
        this.id = id;
    }

    public Partido getPartido(){
        return partido;
    }

    public void setPartido(Partido partido){
        this.partido = partido;
    }

    public String getTipo(){
        return tipo;
    }

    public void setTipo(String tipo){
        this.tipo = tipo;
    }

    public Jugador getAnotador(){
        return anotador;
    }

    public void setAnotador(Jugador anotador){
        this.anotador = anotador;
    }

    public Jugador getAsistidor(){
        return asistidor;
    }

    public void setAsistidor(Jugador asistidor){
        this.asistidor = asistidor;
    }

    public int getPenal(){
        return penal;
    }

    public void setPenal(int penal){
        this.penal = penal;
    }

    public int getMinuto(){
        return minuto;
    }

    public void setMinuto(int minuto){
        this.minuto = minuto;
    }

    public int getTiempo(){
        return tiempo;
    }

    public void setTiempo(int tiempo){
        this.tiempo = tiempo;
    }
}
