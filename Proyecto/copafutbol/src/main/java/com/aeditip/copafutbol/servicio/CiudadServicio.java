package com.aeditip.copafutbol.servicio;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.aeditip.copafutbol.entidad.Ciudad;
import com.aeditip.copafutbol.repositorio.CiudadRepo;

@Service
public class CiudadServicio {
    @Autowired
    private CiudadRepo repo;

    /* Crear */
    public int crearCiudad(Ciudad ciudad){
        return repo.save(ciudad).getId();
    }

    public int crearCiudad(String nombre){
        //Ciudad ciudad = new Ciudad(nombre);
        Ciudad ciudad = new Ciudad();
        ciudad.setNombre(nombre);
        return crearCiudad(ciudad);
    }

    /* Leer */
    public Ciudad leerCiudad(int id){
        return repo.findById(id).orElse(null);
    }

    public List<Ciudad> leerTodasCiudades(){
        return repo.findAll();
    }

    /* Modificar */
    public Ciudad modificarCiudad(Ciudad ciudad, int id){
        if(repo.findById(id).isPresent()){
            ciudad.setId(id);
            return repo.save(ciudad);
        }
        return null;
    }

    /* Borrar físicamente */
    public boolean eliminardeBDaCiudad(int id){
        if(repo.findById(id).isPresent()){
            repo.deleteById(id);
            return true;
        }
        return false;
    }
}
