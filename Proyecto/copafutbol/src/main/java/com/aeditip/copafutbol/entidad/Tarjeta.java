package com.aeditip.copafutbol.entidad;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

@Entity
@Table(name = "tarjeta")
public class Tarjeta {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name="id")
    private int id;

    @ManyToOne
    @JoinColumn(name="id_partido")
    private Partido partido;

    @Column(name="tipo")
    private String tipo;

    @ManyToOne
    @JoinColumn(name="id_jugador")
    private Jugador jugador;

    @Column(name="motivo")
    private String motivo;

    @Column(name="minuto")
    private int minuto;

    @Column(name="tiempo")
    private int tiempo;

    /* Constructores */
    public Tarjeta() {}

    public Tarjeta(int id, Partido partido, String tipo, Jugador jugador,
            String motivo, int minuto, int tiempo) {
        this.id = id;
        this.partido = partido;
        this.tipo = tipo;
        this.jugador = jugador;
        this.motivo = motivo;
        this.minuto = minuto;
        this.tiempo = tiempo;
    }

    /* Encapsulamiento */
    public int getId(){
        return id;
    }

    public void setId(int id){
        this.id = id;
    }

    public Partido getPartido(){
        return partido;
    }

    public void setPartido(Partido partido){
        this.partido = partido;
    }

    public String getTipo(){
        return tipo;
    }

    public void setTipo(String tipo){
        this.tipo = tipo;
    }

    public Jugador getJugador(){
        return jugador;
    }

    public void setJugador(Jugador jugador){
        this.jugador = jugador;
    }

    public String getMotivo(){
        return motivo;
    }

    public void setMotivo(String motivo){
        this.motivo = motivo;
    }

    public int getMinuto(){
        return minuto;
    }

    public void setMinuto(int minuto){
        this.minuto = minuto;
    }

    public int getTiempo(){
        return tiempo;
    }

    public void setTiempo(int tiempo){
        this.tiempo = tiempo;
    }
}
