package com.aeditip.copafutbol.controlador;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import com.aeditip.copafutbol.entidad.Estadio;
import com.aeditip.copafutbol.servicio.EstadioServicio;

@RestController
@RequestMapping("/api/estadios")
public class EstadioControlador {
    @Autowired
    private EstadioServicio servicio;

    /* Leer: GET */
    @RequestMapping(value="{id}", method = RequestMethod.GET)
    public Estadio leerEstadio(@PathVariable int id){
        Estadio estadio = servicio.leerEstadio(id);
        if(estadio == null)
            System.out.println("No se encontró al estadio a buscar.");
        return estadio;
    }

    @RequestMapping(value="", method = RequestMethod.GET)
    public List<Estadio> leerTodosEstadios(){
        return servicio.leerTodosEstadios();
    }
}
