package com.aeditip.copafutbol.servicio;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.aeditip.copafutbol.entidad.Estadio;
import com.aeditip.copafutbol.repositorio.EstadioRepo;

@Service
public class EstadioServicio {
    @Autowired
    private EstadioRepo repo;

    /* Leer */
    public Estadio leerEstadio(int id){
        return repo.findById(id).orElse(null);
    }

    public List<Estadio> leerTodosEstadios(){
        return repo.findAll();
    }
}
