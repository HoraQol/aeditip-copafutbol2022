package com.aeditip.copafutbol.controlador;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.aeditip.copafutbol.entidad.Jugador;
import com.aeditip.copafutbol.servicio.JugadorServicio;

@RestController
@RequestMapping("/api/jugadores")
public class JugadorControlador {
    @Autowired
    private JugadorServicio servicio;

    /* Crear: POST */
    @RequestMapping(value="", method = RequestMethod.POST)
    public Jugador crearJugador(@RequestBody Jugador jugador, @RequestParam String pais){
        Jugador auxJugador = servicio.crearJugador(jugador, pais);
        if(auxJugador.equals(null)){
            System.out.println("El país ingresado no existe en la BD.");
            return null;
        } else return auxJugador;
    }

    /* Leer: GET */
    @RequestMapping(value="{id}", method = RequestMethod.GET)
    public Jugador leerJugador(@PathVariable int id){
        Jugador jugador = servicio.leerJugador(id);
        if(jugador == null)
            System.out.println("No se encontró al jugador a buscar.");
        return jugador;
    }

    @RequestMapping(value="", method = RequestMethod.GET)
    public List<Jugador> leerTodosJugadores(){
        return servicio.leerTodosJugadores();
    }
}
