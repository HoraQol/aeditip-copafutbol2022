package com.aeditip.copafutbol.repositorio;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.aeditip.copafutbol.entidad.Pais;

@Repository
/* <T, ID> */
public interface PaisRepo extends JpaRepository<Pais, String> {
}
