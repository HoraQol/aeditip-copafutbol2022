package com.aeditip.copafutbol.entidad;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name = "pais")
public class Pais {
    @Id
    @Column(name="codigo")
    private String codigo;

    @Column(name="nombre")
    private String nombre;

    @Column(name="grupo")
    private String grupo;

    /* Constructores */
    public Pais() {}

    public Pais(String codigo, String nombre, String grupo) {
        this.codigo = codigo;
        this.nombre = nombre;
        this.grupo = grupo;
    }

    /* Encapsulamiento */
    public String getCodigo(){
        return codigo;
    }

    public void setCodigo(String codigo){
        this.codigo = codigo;
    }

    public String getNombre(){
        return nombre;
    }

    public void setNombre(String nombre){
        this.nombre = nombre;
    }

    public String getGrupo(){
        return grupo;
    }

    public void setGrupo(String grupo){
        this.grupo = grupo;
    }
}
