package com.aeditip.copafutbol.entidad;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

@Entity
@Table(name = "cambio")
public class Cambio {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name="id")
    private int id;

    @ManyToOne
    @JoinColumn(name="id_partido")
    private Partido partido;

    @ManyToOne
    @JoinColumn(name="id_sale")
    private Jugador saliente;

    @ManyToOne
    @JoinColumn(name="id_entra")
    private Jugador entrante;

    @Column(name="minuto")
    private int minuto;

    @Column(name="tiempo")
    private int tiempo;

    /* Constructores */
    public Cambio() {}

    public Cambio(int id, Partido partido, Jugador saliente, Jugador entrante,
            int minuto, int tiempo) {
        this.id = id;
        this.partido = partido;
        this.saliente = saliente;
        this.entrante = entrante;
        this.minuto = minuto;
        this.tiempo = tiempo;
    }

    /* Encapsulamiento */
    public int getId(){
        return id;
    }

    public void setId(int id){
        this.id = id;
    }

    public Partido getPartido(){
        return partido;
    }

    public void setPartido(Partido partido){
        this.partido = partido;
    }

    public Jugador getSaliente(){
        return saliente;
    }

    public void setSaliente(Jugador saliente){
        this.saliente = saliente;
    }

    public Jugador getEntrante(){
        return entrante;
    }

    public void setEntrante(Jugador entrante){
        this.entrante = entrante;
    }

    public int getMinuto(){
        return minuto;
    }

    public void setMinuto(int minuto){
        this.minuto = minuto;
    }

    public int getTiempo(){
        return tiempo;
    }

    public void setTiempo(int tiempo){
        this.tiempo = tiempo;
    }
}
