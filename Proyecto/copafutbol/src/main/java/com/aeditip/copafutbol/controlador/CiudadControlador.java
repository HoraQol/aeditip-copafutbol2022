package com.aeditip.copafutbol.controlador;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import com.aeditip.copafutbol.entidad.Ciudad;
import com.aeditip.copafutbol.servicio.CiudadServicio;

@RestController
@RequestMapping("/api/ciudades")
public class CiudadControlador {
    @Autowired
    private CiudadServicio servicio;

    /* Crear: POST */
    @RequestMapping(value="", method = RequestMethod.POST)
    public int crearCiudad(@RequestBody String nombre){
        return servicio.crearCiudad(nombre);
    }

    /* Leer: GET */
    @RequestMapping(value="{id}", method = RequestMethod.GET)
    public Ciudad leerCiudad(@PathVariable int id){
        Ciudad ciudad = servicio.leerCiudad(id);
        if(ciudad == null)
            System.out.println("No se encontró la ciudad a buscar.");
        return ciudad;
    }

    @RequestMapping(value="", method = RequestMethod.GET)
    public List<Ciudad> leerTodasCiudades(){
        return servicio.leerTodasCiudades();
    }

    /* Modificar: PUT */
    @RequestMapping(value="{id}", method = RequestMethod.PUT)
    public Ciudad modificarCiudad(@RequestBody Ciudad ciudad, @PathVariable int id){
        Ciudad auxCiudad = servicio.modificarCiudad(ciudad, id);
        if(auxCiudad == null)
            System.out.println("No se encontró la ciudad a editar. No se realizó ninguna acción.");
        return auxCiudad;
    }

    /* Borrar físicamente: DELETE */
    @RequestMapping(value="{id}", method = RequestMethod.DELETE)
    public String eliminardeBDaCiudad(@PathVariable int id){
        if(servicio.eliminardeBDaCiudad(id)){
            return "Ciudad con ID = " + id + " eliminada de la base de datos.";
        }
        return "No se encontró la ciudad a eliminar. No se realizó ninguna acción.";
    }
}
