package com.aeditip.copafutbol.entidad;

import java.time.LocalDateTime;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

@Entity
@Table(name = "partido")
public class Partido {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name="id")
    private int id;

    @ManyToOne
    @JoinColumn(name="id_fase")
    private FasePartido fase;

    @ManyToOne
    @JoinColumn(name="id_estadio")
    private Estadio estadio;

    @Column(name="hora_inicio")
    private LocalDateTime horaInicio;

    @ManyToOne
    @JoinColumn(name="id_local")
    private Pais local;

    @ManyToOne
    @JoinColumn(name="id_visitante")
    private Pais visitante;

    @Column(name="jugado")
    private int jugado;

    /* Constructores */
    public Partido() {}

    public Partido(int id, FasePartido fase, Estadio estadio, LocalDateTime horaInicio,
            Pais local, Pais visitante, int jugado) {
        this.id = id;
        this.fase = fase;
        this.estadio = estadio;
        this.horaInicio = horaInicio;
        this.local = local;
        this.visitante = visitante;
        this.jugado = jugado;
    }

    /* Encapsulamiento */
    public int getId(){
        return id;
    }

    public void setId(int id){
        this.id = id;
    }

    public FasePartido getFase(){
        return fase;
    }

    public void setFase(FasePartido fase){
        this.fase = fase;
    }

    public Estadio getEstadio(){
        return estadio;
    }

    public void setEstadio(Estadio estadio){
        this.estadio = estadio;
    }

    public LocalDateTime getHoraInicio(){
        return horaInicio;
    }

    public void setHoraInicio(LocalDateTime horaInicio){
        this.horaInicio = horaInicio;
    }

    public Pais getLocal(){
        return local;
    }

    public void setLocal(Pais local){
        this.local = local;
    }

    public Pais getVisitante(){
        return visitante;
    }

    public void setVisitante(Pais visitante){
        this.visitante = visitante;
    }

    public int getJugado(){
        return jugado;
    }

    public void setJugado(int jugado){
        this.jugado = jugado;
    }
}
