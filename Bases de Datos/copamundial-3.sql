CREATE DATABASE IF NOT EXISTS `copafutbol2022`;
USE `copafutbol2022`;

--
-- Estructura de tabla `pais`
--
DROP TABLE IF EXISTS `pais`;
CREATE TABLE `pais` (
  `codigo` CHAR(3) NOT NULL,
  `nombre` VARCHAR(100) DEFAULT NULL,
  `grupo` VARCHAR(100) DEFAULT NULL,
  PRIMARY KEY (`codigo`)
);
-- Grupo A
INSERT INTO `pais`(codigo, nombre, grupo) VALUES ('QAT', 'Qatar', 'A'),('ECU', 'Ecuador', 'A'),('SEN', 'Senegal', 'A'),('NED', 'Países Bajos', 'A');
-- Grupo B
INSERT INTO `pais`(codigo, nombre, grupo) VALUES ('ENG', 'Inglaterra', 'B'),('IRN', 'Irán', 'B'),('USA', 'Estados Unidos', 'B'),('WAL', 'Gales', 'B');
-- Grupo C
INSERT INTO `pais`(codigo, nombre, grupo) VALUES ('ARG', 'Argentina', 'C'),('KSA', 'Arabia Saudita', 'C'),('MEX', 'México', 'C'),('POL', 'Polonia', 'C');
-- Grupo D
INSERT INTO `pais`(codigo, nombre, grupo) VALUES ('AUS', 'Australia', 'D'),('DEN', 'Dinamarca', 'D'),('FRA', 'Francia', 'D'),('TUN', 'Túnez', 'D');
-- Grupo E
INSERT INTO `pais`(codigo, nombre, grupo) VALUES ('CRC', 'Costa Rica', 'E'),('GER', 'Alemania', 'E'),('JPN', 'Japón', 'E'),('SPA', 'España', 'E');
-- Grupo F
INSERT INTO `pais`(codigo, nombre, grupo) VALUES ('BEL', 'Bélgica', 'F'),('CRO','Croacia', 'F'),('MOR', 'Marruecos', 'F'),('CAN', 'Canadá', 'F');
-- Grupo G
INSERT INTO `pais`(codigo, nombre, grupo) VALUES ('BRA', 'Brasil', 'G'),('CMR', 'Camerún', 'G'),('SRB', 'Serbia', 'G'),('SWI', 'Suiza', 'G');
-- Grupo H
INSERT INTO `pais`(codigo, nombre, grupo) VALUES ('GHA', 'Ghana', 'H'),('POR', 'Portugal', 'H'),('KOR', 'Corea del Sur', 'H'),('URU', 'Uruguay', 'H');

--
-- Estructura de tabla `ciudad`
--
DROP TABLE IF EXISTS `ciudad`;
CREATE TABLE `ciudad` (
  `id` INT NOT NULL AUTO_INCREMENT,
  `nombre` VARCHAR(100) DEFAULT NULL,
  PRIMARY KEY (`id`)
);
INSERT INTO `ciudad`(id, nombre) VALUES (1, 'Lusail'),(2, 'Al Khor'),(3, 'Doha'),(4, 'Al Rayyan'),(5, 'Al Wakrah');

--
-- Estructura de tabla `estadio`
--
DROP TABLE IF EXISTS `estadio`;
CREATE TABLE `estadio` (
  `id` INT NOT NULL AUTO_INCREMENT,
  `nombre` VARCHAR(100) DEFAULT NULL,
  `id_ciudad` INT DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `estadio_fk01` (`id_ciudad`),
  CONSTRAINT `estadio_fk01` FOREIGN KEY (`id_ciudad`) REFERENCES `ciudad` (`id`)
);
INSERT INTO `estadio`(id, nombre, id_ciudad) VALUES (1, 'Lusail Iconic Stadium', 1),(2, 'Al Bayt Stadium', 2),(3, 'Al Thumama Stadium', 3),(4, 'Stadium 974', 3),(5, 'Khalifa International Stadium', 4),(6, 'Ahmad bin Ali Stadium', 4),(7, 'Education City Stadium', 4),(8, 'Al Janoub Stadium', 5);

--
-- Estructura de tabla `fase_partido`
--
DROP TABLE IF EXISTS `fase_partido`;
CREATE TABLE `fase_partido` (
  `id` INT NOT NULL AUTO_INCREMENT,
  `nombre` VARCHAR(100) DEFAULT NULL,
  PRIMARY KEY (`id`)
);

INSERT INTO `fase_partido`(id, nombre) VALUES (1, 'Fase de Grupos'),(2, 'Octavos de Final'),(3, 'Cuartos de Final'),(4, 'Semifinal'),(5, 'Tercer Puesto'),(6, 'Final');

--
-- Estructura de tabla `jugador`
--
DROP TABLE IF EXISTS `jugador`;
CREATE TABLE `jugador` (
  `id` INT NOT NULL AUTO_INCREMENT,
  `nombre` VARCHAR(100) DEFAULT NULL,
  `dorsal` INT DEFAULT NULL,
  `posicion` VARCHAR(3) DEFAULT NULL,
  `id_pais` CHAR(3) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `jugador_fk01` (`id_pais`),
  CONSTRAINT `jugador_fk01` FOREIGN KEY (`id_pais`) REFERENCES `pais` (`codigo`)
);
-- Faltan todos los jugadores.
-- Comprender que son 26 jugadores por país y 32 países = 832 jugadores!!!

-- Jugadores de Qatar
INSERT INTO `jugador`(id, nombre, dorsal, posicion, id_pais) VALUES (1, 'Saad Al Sheeb', 1, 'POR', 'QAT'),(4, 'Mohammed Waad', 4, 'CEN', 'QAT'),(9, 'Mohammed Muntari', 9, 'DEL', 'QAT'),(10, 'Hassan Al-Haidos', 10, 'DEL', 'QAT'),(11, 'Akram Afif', 11, 'DEL', 'QAT'),(12, 'Karim Boudiaf', 12, 'CEN', 'QAT'),(19, 'Almoez Ali', 19, 'DEL', 'QAT');
-- Jugadores de Ecuador
INSERT INTO `jugador`(id, nombre, dorsal, posicion, id_pais) VALUES (27, 'Enner Valencia', 13, 'DEL', 'ECU'),(28, 'Angelo Preciado', 17, 'DEF', 'ECU'),(29, 'Jhegson Méndez', 20, 'CEN', 'ECU'),(30, 'Romario Ibarra', 10, 'CEN', 'ECU'),(31, 'Jeremy Sarmiento', 16, 'CEN', 'ECU'),(32, 'José Cifuentes', 5, 'CEN', 'ECU'),(33, 'Kevin Rodríguez', 26, 'CEN', 'ECU'),(34, 'Michael Estrada', 11, 'DEL', 'ECU'),(35, 'Alan Franco', 21, 'CEN', 'ECU'),(36, 'Moisés Caicedo', 23, 'CEN', 'ECU'); 
--
-- Estructura de tabla `partido`
--
DROP TABLE IF EXISTS `partido`;
CREATE TABLE `partido` (
  `id` INT NOT NULL AUTO_INCREMENT,
  `id_fase` INT NOT NULL,
  `id_estadio` INT NOT NULL,
  `hora_inicio` DATETIME NOT NULL,
  `id_local` CHAR(3) DEFAULT NULL,
  `id_visitante` CHAR(3) DEFAULT NULL,
  `jugado` INT DEFAULT 0,
  PRIMARY KEY (`id`),
  KEY `partido_fk01` (`id_fase`),
  KEY `partido_fk02` (`id_estadio`),
  KEY `partido_fk03` (`id_local`),
  KEY `partido_fk04` (`id_visitante`),
  CONSTRAINT `partido_fk01` FOREIGN KEY (`id_fase`) REFERENCES `fase_partido` (`id`),
  CONSTRAINT `partido_fk02` FOREIGN KEY (`id_estadio`) REFERENCES `estadio` (`id`),
  CONSTRAINT `partido_fk03` FOREIGN KEY (`id_local`) REFERENCES `pais` (`codigo`),
  CONSTRAINT `partido_fk04` FOREIGN KEY (`id_visitante`) REFERENCES `pais` (`codigo`)
);

-- FASE DE GRUPOS
-- Grupo A
INSERT INTO `partido`(id, id_fase, id_estadio, hora_inicio, id_local, id_visitante) VALUES (1, 1, 2, '2022-11-20 11:00:00', 'QAT', 'ECU'), (2, 1, 3, '2022-11-21 11:00:00', 'SEN', 'NED'),(17, 1, 3, '2022-11-25 08:00:00', 'QAT', 'SEN'), (18, 1, 5, '2022-11-25 11:00:00', 'NED', 'ECU'),(33, 1, 5, '2022-11-29 10:00:00', 'NED', 'QAT'), (34, 1, 2, '2022-11-29 10:00:00', 'ECU', 'SEN');
-- Grupo B
INSERT INTO `partido`(id, id_fase, id_estadio, hora_inicio, id_local, id_visitante) VALUES (3, 1, 5, '2022-11-21 08:00:00', 'ENG', 'IRN'), (4, 1, 6, '2022-11-21 14:00:00', 'USA', 'WAL'),(19, 1, 6, '2022-11-25 05:00:00', 'WAL', 'IRN'), (20, 1, 2, '2022-11-25 14:00:00', 'ENG', 'USA'),(35, 1, 6, '2022-11-29 14:00:00', 'WAL', 'ENG'), (36, 1, 3, '2022-11-29 14:00:00', 'IRN', 'USA');
-- Grupo C
INSERT INTO `partido`(id, id_fase, id_estadio, hora_inicio, id_local, id_visitante) VALUES (5, 1, 5, '2022-11-22 05:00:00', 'ARG', 'KSA'), (6, 1, 6, '2022-11-22 11:00:00', 'MEX', 'POL'),(21, 1, 6, '2022-11-26 08:00:00', 'POL', 'KSA'), (22, 1, 2, '2022-11-26 14:00:00', 'ARG', 'MEX'),(37, 1, 6, '2022-11-30 14:00:00', 'POL', 'ARG'), (38, 1, 3, '2022-11-30 14:00:00', 'KSA', 'MEX');
-- Faltan D-H y fases finales

--
-- Estructura de tabla `gol`
--
DROP TABLE IF EXISTS `gol`;
CREATE TABLE `gol` (
  `id` INT NOT NULL AUTO_INCREMENT,
  `id_partido` INT NOT NULL,
  -- G: Gol | A: Autogol
  `tipo` CHAR(1) NOT NULL,
  `id_anotador` INT NOT NULL,
  `id_asistidor` INT DEFAULT NULL,
  `penal` INT DEFAULT 0,
  `minuto` INT NOT NULL,
  `tiempo` INT NOT NULL,
  PRIMARY KEY (`id`),
  KEY `gol_fk01` (`id_partido`),
  KEY `gol_fk02` (`id_anotador`),
  KEY `gol_fk03` (`id_asistidor`),
  CONSTRAINT `gol_fk01` FOREIGN KEY (`id_partido`) REFERENCES `partido` (`id`),
  CONSTRAINT `gol_fk02` FOREIGN KEY (`id_anotador`) REFERENCES `jugador` (`id`),
  CONSTRAINT `gol_fk03` FOREIGN KEY (`id_asistidor`) REFERENCES `jugador` (`id`)
);

--
-- Estructura de tabla `tarjeta`
--
DROP TABLE IF EXISTS `tarjeta`;
CREATE TABLE `tarjeta` (
  `id` INT NOT NULL AUTO_INCREMENT,
  `id_partido` INT NOT NULL,
  -- A: Amarilla | R: Roja
  `tipo` CHAR(1) NOT NULL,
  `id_jugador` INT NOT NULL,
  `motivo` VARCHAR(30) NOT NULL,
  `minuto` INT NOT NULL,
  `tiempo` INT NOT NULL,
  PRIMARY KEY (`id`),
  KEY `tarjeta_fk01` (`id_partido`),
  KEY `tarjeta_fk02` (`id_jugador`),
  CONSTRAINT `tarjeta_fk01` FOREIGN KEY (`id_partido`) REFERENCES `partido` (`id`),
  CONSTRAINT `tarjeta_fk02` FOREIGN KEY (`id_jugador`) REFERENCES `jugador` (`id`)
);

--
-- Estructura de tabla `cambio`
--
DROP TABLE IF EXISTS `cambio`;
CREATE TABLE `cambio` (
  `id` INT NOT NULL AUTO_INCREMENT,
  `id_partido` INT NOT NULL,
  `id_sale` INT NOT NULL,
  `id_entra` INT NOT NULL,
  `minuto` INT NOT NULL,
  `tiempo` INT NOT NULL,
  PRIMARY KEY (`id`),
  KEY `cambio_fk01` (`id_partido`),
  KEY `cambio_fk02` (`id_sale`),
  KEY `cambio_fk03` (`id_entra`),
  CONSTRAINT `cambio_fk01` FOREIGN KEY (`id_partido`) REFERENCES `partido` (`id`),
  CONSTRAINT `cambio_fk02` FOREIGN KEY (`id_sale`) REFERENCES `jugador` (`id`),
  CONSTRAINT `cambio_fk03` FOREIGN KEY (`id_entra`) REFERENCES `jugador` (`id`)
);

-- Nota mental del autor: En el partido Qatar vs. Ecuador pasó lo siguiente:
-- 15' Tarjeta amarilla para Saad Al Sheeb por falta en área
-- 16' Gol de Enner Valencia de penal
-- 22' Tarjeta amarilla para Almoez Ali por infracción
-- 29' Tarjeta amarilla para Enner Valencia por infracción
-- 31' Gol de Enner Valencia (asistencia de Angelo Preciado)
-- 36' Tarjeta amarilla para Karim Boudiaf por infracción
-- Segundo tiempo
-- 11' Tarjeta amarilla para Jhegson Méndez por infracción
-- 23' Cambio: Jeremy Sarmiento por Romario Ibarra
-- 26' Cambio: Mohammed Waad por Hassan Al-Haidos
-- 27' Cambio: Mohammed Muntari por Almoez Ali
-- 32' Cambio: José Cifuentes por Enner Valencia
-- 33' Tarjeta amarilla para Akram Afif por infracción
-- 45' Cambio: Kevin Rodríguez por Michael Estrada
-- 45' Cambio: Alan Franco por Moisés Caicedo