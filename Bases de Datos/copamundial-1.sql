CREATE DATABASE IF NOT EXISTS `copamundial2022`;
USE `copamundial2022`;

--
-- Estructura de tabla `pais`
--
DROP TABLE IF EXISTS `pais`;
CREATE TABLE `pais` (
  `codigo` CHAR(3) NOT NULL,
  `nombre` VARCHAR(100) DEFAULT NULL,
  PRIMARY KEY (`codigo`)
);
-- Grupo A
INSERT INTO `pais`(codigo, nombre) VALUES ('QAT', 'Qatar'),('ECU', 'Ecuador'),('SEN', 'Senegal'),('NED', 'Países Bajos');
-- Grupo B
INSERT INTO `pais`(codigo, nombre) VALUES ('ENG', 'Inglaterra'),('IRN', 'Irán'),('USA', 'Estados Unidos'),('WAL', 'Gales');
-- Grupo C
INSERT INTO `pais`(codigo, nombre) VALUES ('ARG', 'Argentina'),('KSA', 'Arabia Saudita'),('MEX', 'México'),('POL', 'Polonia');
-- Grupo D
INSERT INTO `pais`(codigo, nombre) VALUES ('AUS', 'Australia'),('DEN', 'Dinamarca'),('FRA', 'Francia'),('TUN', 'Túnez');
-- Grupo E
INSERT INTO `pais`(codigo, nombre) VALUES ('CRC', 'Costa Rica'),('GER', 'Alemania'),('JPN', 'Japón'),('SPA', 'España');
-- Grupo F
INSERT INTO `pais`(codigo, nombre) VALUES ('BEL', 'Bélgica'),('CRO','Croacia'),('MOR', 'Marruecos'),('CAN', 'Canadá');
-- Grupo G
INSERT INTO `pais`(codigo, nombre) VALUES ('BRA', 'Brasil'),('CMR', 'Camerún'),('SRB', 'Serbia'),('SWI', 'Suiza');
-- Grupo H
INSERT INTO `pais`(codigo, nombre) VALUES ('GHA', 'Ghana'),('POR', 'Portugal'),('KOR', 'Corea del Sur'),('URU', 'Uruguay');

--
-- Estructura de tabla `estadio`
--
DROP TABLE IF EXISTS `estadio`;
CREATE TABLE `estadio` (
  `id` INT NOT NULL AUTO_INCREMENT,
  `nombre` VARCHAR(100) DEFAULT NULL,
  `ciudad` VARCHAR(100) DEFAULT NULL,
  PRIMARY KEY (`id`)
);
INSERT INTO `estadio`(id, nombre, ciudad) VALUES (1, 'Lusail Iconic Stadium', 'Lusail'),(2, 'Al Bayt Stadium', 'Al Khor'),(3, 'Al Thumama Stadium', 'Doha'),(4, 'Stadium 974', 'Doha'),(5, 'Khalifa International Stadium', 'Al Rayyan'),(6, 'Ahmad bin Ali Stadium', 'Al Rayyan'),(7, 'Education City Stadium', 'Al Rayyan'),(8, 'Al Janoub Stadium', 'Al Wakrah');

--
-- Estructura de tabla `fase_partido`
--
DROP TABLE IF EXISTS `fase_partido`;
CREATE TABLE `fase_partido` (
  `id` INT NOT NULL AUTO_INCREMENT,
  `nombre` VARCHAR(100) DEFAULT NULL,
  PRIMARY KEY (`id`)
);

INSERT INTO `fase_partido`(id, nombre) VALUES (1, 'Fase de Grupos'),(2, 'Octavos de Final'),(3, 'Cuartos de Final'),(4, 'Semifinal'),(5, 'Tercer Puesto'),(6, 'Final');

--
-- Estructura de tabla `partido`
--
DROP TABLE IF EXISTS `partido`;
CREATE TABLE `partido` (
  `id` INT NOT NULL AUTO_INCREMENT,
  `id_fase` INT NOT NULL,
  `id_estadio` INT NOT NULL,
  `id_local` CHAR(3) DEFAULT NULL,
  `id_visitante` CHAR(3) DEFAULT NULL,
  `marcador_local` INT DEFAULT 0,
  `amarilla_local` INT DEFAULT 0,
  `segunda_local` INT DEFAULT 0,
  `roja_local` INT DEFAULT 0,
  `marcador_visitante` INT DEFAULT 0,
  `amarilla_visitante` INT DEFAULT 0,
  `segunda_visitante` INT DEFAULT 0,
  `roja_visitante` INT DEFAULT 0,
  `jugado` INT DEFAULT 0,
  PRIMARY KEY (`id`),
  KEY `partido_fk01` (`id_fase`),
  KEY `partido_fk02` (`id_estadio`),
  KEY `partido_fk03` (`id_local`),
  KEY `partido_fk04` (`id_visitante`),
  CONSTRAINT `partido_fk01` FOREIGN KEY (`id_fase`) REFERENCES `fase_partido` (`id`),
  CONSTRAINT `partido_fk02` FOREIGN KEY (`id_estadio`) REFERENCES `estadio` (`id`),
  CONSTRAINT `partido_fk03` FOREIGN KEY (`id_local`) REFERENCES `pais` (`codigo`),
  CONSTRAINT `partido_fk04` FOREIGN KEY (`id_visitante`) REFERENCES `pais` (`codigo`)
);

-- FASE DE GRUPOS
-- Grupo A
INSERT INTO `partido`(id, id_fase, id_estadio, id_local, id_visitante) VALUES (1, 1, 2, 'QAT', 'ECU'), (2, 1, 3, 'SEN', 'NED'),(17, 1, 3, 'QAT', 'SEN'), (18, 1, 5, 'NED', 'ECU'),(33, 1, 5, 'NED', 'QAT'), (34, 1, 2, 'ECU', 'SEN');
-- Grupo B
INSERT INTO `partido`(id, id_fase, id_estadio, id_local, id_visitante) VALUES (3, 1, 5, 'ENG', 'IRN'), (4, 1, 6, 'USA', 'WAL'),(19, 1, 6, 'WAL', 'IRN'), (20, 1, 2, 'ENG', 'USA'),(35, 1, 6, 'WAL', 'ENG'), (36, 1, 3, 'IRN', 'USA');
-- Grupo C
INSERT INTO `partido`(id, id_fase, id_estadio, id_local, id_visitante) VALUES (5, 1, 5, 'ARG', 'KSA'), (6, 1, 6, 'MEX', 'POL'),(21, 1, 6, 'POL', 'KSA'), (22, 1, 2, 'ARG', 'MEX'),(37, 1, 6, 'POL', 'ARG'), (38, 1, 3, 'KSA', 'MEX');
-- Faltan D-H y fases finales

--
-- Estructura de tabla `jugador`
--
DROP TABLE IF EXISTS `jugador`;
CREATE TABLE `jugador` (
  `id` INT NOT NULL AUTO_INCREMENT,
  `nombre` VARCHAR(100) DEFAULT NULL,
  `dorsal` INT DEFAULT NULL,
  `posicion` VARCHAR(3) DEFAULT NULL,
  `id_pais` CHAR(3) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `jugador_fk01` (`id_pais`),
  CONSTRAINT `jugador_fk01` FOREIGN KEY (`id_pais`) REFERENCES `pais` (`codigo`)
);
-- Faltan todos los jugadores.
-- Comprender que son 26 jugadores por país y 32 países = 832 jugadores!!!